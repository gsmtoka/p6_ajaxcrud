<?php
//index.php
include 'crud.php';
$object = new Crud();
?>

<html>
	<head>
		<title>PHP Mysql Ajax Crud using OOPS - Fetch Data</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

		<style>
			body
			{
				margin: 0;
				padding: 0;
				background-color: #f1f1f1;
			}

			.box
			{
				width: 900px;
				padding: 20px;
				background-color: #fff;
				border: 1px solid #ccc;
				border-radius: 5px;
				margin-top: 100px;
			}
		</style>
	</head>

	<body>

		<div class="container box" align="center">
			<h2> <a href="https://www.youtube.com/watch?v=7imibrpUoc0" target="blank">MINAM SANAM SHECDOMA GASWORDEBA - DATKBIT</a> 
				<br/>გ ი ლ ო ც ა ვ თ &nbsp &nbsp &nbsp მ ა რ ი ა მ ო ბ ა ს !</h2>
		</div>
		<div class="container box">
			<h3 align="center">PHP Mysql Ajax Crud using OOPS - Fetch Data</h3><br/>
			<button type="button" name="add" class="btn btn-success" data-toggle="collapse" data-target="#user_collapse">Add</button>
			<br/><br/>
			
			<div id="user_collapse" class="collapse">
				<form method="POST" id="user_form" enctype="multipart/form-data" action="action.php"> 
					<label>Enter First Name</label>
					<input type="text" name="first_name" id="first_name" class="form-control" />
					<br/>

					<label>Enter Last Name</label>
					<input type="text" name="last_name" id="last_name" class="form-control" />
					<br/>					

					<label>Select User Image</label>
					<input type="file" name="user_image" id="user_image" />
					<input type="hidden" name="hidden_user_image" id="hidden_user_image" />
					<span id="uploaded_image"></span>
					<br/>					

					<div align="center">
						<input type="hidden" name="action" id="action" />
						<input type="hidden" name="user_id" id="user_id" />
						<input type="submit" name="button_action" class="btn btn-default" id="button_action" value="Insert" />
					</div>
				</form>
			</div>

			<div id="user_table" class="table-responsive">
			</div>
		</div>
	</body>
</html>

<script type="text/javascript">
	$(document).ready(function(){

		load_data();

		function load_data()
		{
			var action = "Load";
			$.ajax({
				url:"action.php",
				method:"POST",
				data:{action:action},
				success:function(data)
				{
					$('#user_table').html(data);
				}
			});
		}

		$('#user_form').on('submit', function(event){
			event.preventDefault();
			var firstname = $('#first_name').val();
			var lastname = $('#last_name').val();
			var extension = $('#user_image').val().split('.').pop().toLowerCase();
			if(extension != '')
			{
				if(jQuery.inArray(extension, ['gif', 'png', 'jpg', 'jpeg']) == -1)
				{
					alert("Invalid Image File");
					$('#user_image').val('');
					return false;
				}
			}
			if(firstname != '' && lastname != '')
			{
				$.ajax({
					url:"action.php",
					method:"POST",
					data:new FormData(this),
					contentType:false,
					processData:false,
					success:function(data)
					{
						alert(data);
						$('#user_form')[0].reset();
						load_data();
						$("#action").val("Insert");
						$('#button_action').val("Insert");
						$('#uploaded_image').html('');
					}
				})
			}
			else
			{
				alert("Both Fields are Required");
			}
		});

		$(document).on('click', '.update', function(){
			var user_id = $(this).attr("id");
			var action = "Fetch Single Data";
			$.ajax({
				url:"action.php",
				method:"POST",
				data:{user_id:user_id, action:action},
				dataType:"json",
				success:function(data)
				{
					$('.collapse').collapse("show");
					$('#first_name').val(data.first_name);
					$('#last_name').val(data.last_name);
					$('#uploaded_image').html(data.image);
					$('#hidden_user_image').val(data.user_image);
					$('#button_action').val("Edit");
					$('#action').val("Edit");
					$('#user_id').val(user_id);
				}
			})
		})

	});
</script>